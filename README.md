## Repositório para criação dos textos e artigos presentes no site https://monerobrasil.org

A intenção deste site é a colaboração entre os membros da comunidade Monero no Brasil e demais países falantes da língua portuguesa. Caso queira contribuir enviando algum artigo ou tradução, é muito simples. Basta seguir os passos descritos neste post.

Todos os artigos do Monero Brasil estão hospedados em um repositório no GitLab chamado [artigos-MoneroBrasil.org](https://gitlab.com/netrik182/artigos-monerobrasil.org) para acompanhamento e publicação do conteúdo. 

Para mais informações, acesse nossa página e contribua você também: https://monerobrasil.org/artigos/contribua/
