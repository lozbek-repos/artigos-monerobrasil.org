---
title: "XMR.to"
description: "Como usar a corretora instantânea XMR.to para pagar com Monero"
date: 2020-05-16T17:15:08-03:00
draft: true
tags:
  - corretora
  -
categories:
  - todos
  - corretoras
  - guias
  -
image: "thumbnail.svg"
author: ""
toc: false
---

> As empresas e/ou serviços listados nessa página não são endossadas pelo site MoneroBrasil.org. Sempre que for utilizar um serviço, pesquise a respeito de sua reputação e utilize-o apenas se entender seus riscos por completo.
