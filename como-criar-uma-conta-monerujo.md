---
title: "Como criar um endereço e usar a carteira Monerujo"
description: "Passo-a-passo de como usar a carteira Monerujo e criar uma conta Monero."
date: 2020-06-10T18:23:52-03:00
draft: false
tags:
  - guia
  - carteira
  - 
categories:
  - todos
  - guias
  - carteiras
  -
image: "capa-monerujo.jpg"
author: "kyoto"
toc: true
---

A Monerujo foi uma das primeiras carteiras Monero (se não a primeira de fato) criadas para Android e é totalmente recomendada pela comunidade internacional do Monero e também por nós aqui do site Monero Brasil.

Neste post você encontrará um guia passo-a-passo de como instalar e configurar sua própria carteira Monero!

## Instalar a Carteira Monerujo

Navegue até a Google Play em seu celular e pesquise por "Monerujo". Se preferir, você pode clicar neste botão abaixo para ser redirecionado. Em seguida, clique em "Instalar" e abra o aplicativo.

{{< figure src="/images/google-play-badge.png" alt="Google Play" width="30%" class="center" link="https://play.google.com/store/apps/details?id=com.m2049r.xmrwallet" >}}

{{< figure src="/images/instalar-monerujo.png" alt="Instalar Monerujo" width="100%" class="center" link="https://play.google.com/store/apps/details?id=com.m2049r.xmrwallet" >}}

### Adicionar um nó remoto

Clique no botão "Toque para selecionar ou adicionar nós":

{{< figure src="/images/monerujo_01.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Em seguida, clique em "+ADICIONAR NÓ" para adicionar um nó manualmente ou puxe para baixo para escanear os nós disponíveis próximos a você.

{{< figure src="/images/monerujo_02.png" alt="Carteira Monerujo" width="35%" class="center" >}}

O app irá mostrar todos os nós disponíveis, conforme imagem abaixo.

{{< figure src="/images/monerujo_03.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Selecione de 4 a 5 nós no topo da lista clicando no ícone à esquerda.

Dessa forma, a Monerujo irá selecionar automaticamente um nó dentre os selecionados que tenha a melhor conexão e esteja ativo no momento.

Após esses passos, retorne à tela inicial.

### Criar uma nova carteira

{{< figure src="/images/monerujo_04.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Clique no botão com o símbolo "+" para abrir as opções de criação de carteira.

{{< figure src="/images/monerujo_05.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Você poderá criar uma nova carteira ou restaurar uma já existente.

Neste guia iremos criar uma nova carteira clicando no botão "Criar nova carteira", no topo da lista.

{{< figure src="/images/monerujo_06.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Dê um nome e uma senha a sua carteira.

Marque a opção "Permitir abrir carteira com impressão digital" para acessar sua carteira com impressão digital, se preferir.

{{< figure src="/images/monerujo_07.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Leia a mensagem e o aviso de segurança. Se estiver de acordo, clique em "OK".

{{< figure src="/images/monerujo_08.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Com todas as informações inseridas, clique em "VAMOS CRIAR ESSA CARTEIRA LOGO".

{{< figure src="/images/monerujo_09.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Aguarde a criação da carteira, o processo poderá levar alguns segundos dependendo o seu celular.

{{< figure src="/images/monerujo_10.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Faça backup da **Semente mnemônica** de preferência de forma offline (usando folha de papel, por exemplo) e guarde com segurança.

**NÃO PULE ESSA ETAPA! SE VOCÊ PERDER SEU CELULAR OU ESQUECER SUA SENHA, VOCÊ PERDERÁ TODO SEU MONERO E NÃO SERÁ POSSÍVEL USAR SUA DIGITAL PARA RECURAR SEU DINHEIRO**

Após anotar sua semente mnemônica, clique em "EU ANOTEI MINHA SEMENTE MNEMÔNICA".

{{< figure src="/images/monerujo_11.png" alt="Carteira Monerujo" width="35%" class="center" >}}

A carteira está pronta para uso. Você poderá criar quantas carteiras forem necessárias repetindo os passos anteriores.

## Acessando a carteira

Clique no nome da carteira para acessá-la.

{{< figure src="/images/monerujo_12.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Será solicitada a senha ou a autenticação pela impressão digital.

{{< figure src="/images/monerujo_13.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Com a carteira aberta, ela irá se conectar ao nó configurado anteriormente.

{{< figure src="/images/monerujo_14.png" alt="Carteira Monerujo" width="35%" class="center" >}}

A carteira vai escanear os blocos. Enquanto isso já é possível receber Monero, mas enviar somente ao final do escaneamento.

Lembre-se que este escaneamento não é o download da blockchain, já que estamos usado um nó remoto.

{{< figure src="/images/monerujo_15.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Escaneamento concluído e a carteira está sincronizada.

{{< figure src="/images/monerujo_16.png" alt="Carteira Monerujo" width="35%" class="center" >}}

### Recebendo Monero

Clique em "Receber" para visualizar o QR Code do seu endereço.

{{< figure src="/images/monerujo_17.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Há a possibilidade de cotar os valores em diversas moedas fiduciárias, como Real, Dólar e Euro, por exemplo.

{{< figure src="/images/monerujo_18.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Assim, pode-se inserir o valor da transação em moeda fiduciária:

{{< figure src="/images/monerujo_19.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Ou em Monero:

{{< figure src="/images/monerujo_20.png" alt="Carteira Monerujo" width="35%" class="center" >}}

### Enviando Monero

Para enviar, pode-se colar o endereço do destinatário ou escanear o QR Code.

{{< figure src="/images/monerujo_22.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Após isso é só inserir a quantia de Monero que será enviada e confirmar. Você poderá confirmar os dados inseridos e deverá inserir sua senha.

Lembre-se que, por questões de segurança, não é possível enviar Monero usando sua digital.

{{< figure src="/images/monerujo_23.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Ao clicar na cabeça do Gunther pode ser ativado/desativado o **Modo Rua**, onde seu saldo não será exibido na tela.

Este modo é útil para usar sua carteira em locais públicos, por exemplo, sem que as pessoas a sua volta possam ver quanto Monero você tem.

{{< figure src="/images/monerujo_24.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Clicando no ícone dos três pontinhos ao lado da cabeça do Gunter, é possível ver algumas opções, dentre elas "Mostrar Segredos!".

{{< figure src="/images/monerujo_25.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Clicando nesta opção um alerta de segurança aparecerá. Confirme se está realmente seguro, pois o app irá mostrar as chaves secretas e a semente mnemônica da carteira. Não permita que ninguém visualize a tela de seu smartphone enquanto estiver usando esta opção.

{{< figure src="/images/monerujo_26.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Tendo retornado à tela inicial da carteira, pode-se adicionar outra carteira clicando no símbolo de "+".

{{< figure src="/images/monerujo_27.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Repita os procedimentos de criação de carteira mostrados anteriormente.

{{< figure src="/images/monerujo_28.png" alt="Carteira Monerujo" width="35%" class="center" >}}

E uma nova carteira estará pronta para ser usada.

{{< figure src="/images/monerujo_29.png" alt="Carteira Monerujo" width="35%" class="center" >}}

Agora que você tem uma carteira Monero já pode começar a transferir seus moneroj para ela. E se você ainda não possui nenhum, confira nosso guia de [como comprar Monero usando Bitcoin, Litecoin ou qualquer outra criptomoeda](../changenowio/).
