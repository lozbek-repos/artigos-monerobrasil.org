---
title: "Como verificar a assinatura dos arquivos do Monero"
description: "Guia passo-a-passo e bem detalhado de como verificar a assinatura dos arquivos Monero sempre que realizar o download de uma nova versão."
date: 2020-06-07T12:31:04-03:00
draft: false
tags:
  - guia
  -
categories:
  - todos
  - guias
  - 
image: "security.jpg"
author: ""
toc: true
---

A verificação dos arquivos binários deve sempre ser feita antes de se extrair, instalar e usar o software do Monero. Essa é a única maneira de assegurar que você está usando o software oficial do Monero. Com esse guia você aprenderá como não ser enganado a usar um programa falso do Monero que você venha a baixar em um phishing, em um ataque hacker, etc.

Para proteger a integridade dos binários, a equipe Monero fornece uma lista assinada criptograficamente contendo todos os hashes [SHA256](https://pt.wikipedia.org/wiki/SHA-2) dos binários. Se o binário que você baixou foi adulterado, ele irá produzir um [hash diferente](https://pt.wikipedia.org/wiki/File_hashing) do que o hash esperado para o arquivo.

Esse é um guia avançado para os sistemas operacionais Linux, Mac e Windows e, portanto, você precisará saber como usar a linha de comando. Neste guia você aprenderá como instalar o software necessário, como importar a chave de assinatura, como baixar os arquivos necessários e como verificar se o seu binário é autêntico.

## 1. Instalando o GnuPG

+ No Windows, vá para a [página de download do Gpg4win](https://gpg4win.org/download.html) e siga as instruções para a instalação.

+ No Mac, vá para a [página de download do Gpgtools](https://gpgtools.org/) e siga as instruções para a instalação.

+ No Linux, o GnuPG já vem instalado por padrão.

## 2. Verificar e importar a chave de assinatura

Nessa seção você irá aprender como adquirir a chave de assinatura do Monero, como certificar-se de que você baixou a chave correta e como importar a chave para o GnuPG.

### 2.1. Baixando a chave de assinatura

No Windows ou Mac, vá para a página com a [chave GPG do binaryFate](https://raw.githubusercontent.com/monero-project/monero/master/utils/gpg_keys/binaryfate.asc), que ele usa para assinar os binários do Monero, e salve-a como um arquivo `binaryfate.asc` em seu computador.

No Linux, você pode baixar a chave de assinatura do binaryFate através do seguinte comando:

```bash
wget -O binaryfate.asc https://raw.githubusercontent.com/monero-project/monero/master/utils/gpg_keys/binaryfate.asc
```

### 2.2. Verificando a chave de assinatura

Em todos os sistemas operacionais, verifique a impressão digital (fingerprint) do arquivo `binaryfate.asc` executando o seguinte comando em um terminal:

```bash
gpg --keyid-format long --with-fingerprint binaryfate.asc
```


Verifique se a impressão digital do seu arquivo corresponde à impressão digital abaixo:

```bash
pub   rsa4096/F0AF4D462A0BDF92 2019-12-12 [SCEA]
      Key fingerprint = 81AC 591F E9C4 B65C 5806  AFC3 F0AF 4D46 2A0B DF92
uid                           binaryFate <binaryfate@getmonero.org>
```

Se a impressão digital **CORRESPONDE**, então você pode continuar.

Se a impressão digital **NÃO CORRESPONDE**, **NÃO CONTINUE** a instalação. Delete o arquivo `binaryfate.asc` e volte para a [seção 2.1](#21-baixando-a-chave-de-assinatura).

### 2.3. Importando a chave de assinatura

Em um terminal, importe a chave de assinatura:

```bash
gpg --import binaryfate.asc
```

Se essa for a primeira vez que você estiver importando a chave, o resultado será algo parecido com isso:

```bash
gpg: key F0AF4D462A0BDF92: 2 signatures not checked due to missing keys
gpg: key F0AF4D462A0BDF92: public key "binaryFate <binaryfate@getmonero.org>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: marginals needed: 3  completes needed: 1  trust model: pgp
```

Se você já importou a chave anteriormente, o resultado será algo parecido com isso:

```bash
gpg: key F0AF4D462A0BDF92: "binaryFate <binaryfate@getmonero.org>" not changed
gpg: Total number processed: 1
gpg:              unchanged: 1
```

## 3. Baixar e verificar o arquivo de hashes

Nessa seção você irá aprender como baixar o arquivo de hashes e como verificar a sua autenticidade.

### 3.1. Baixar o arquivo de hashes

No Windows ou Mac, vá para a página com o [arquivo de hashes em getmonero.org](https://getmonero.org/downloads/hashes.txt) e salve-a como um arquivo `hashes.txt` em seu computador.

No Linux, você pode baixar o arquivo com os hashes assinados através do seguinte comando:

```bash
wget -O hashes.txt https://getmonero.org/downloads/hashes.txt
```

### 3.2. Verificar o arquivo de hashes

O arquivo de hashes é assinado com a chave `81AC 591F E9C4 B65C 5806  AFC3 F0AF 4D46 2A0B DF92` conforme exibido no resultado abaixo.

Em todos os sistemas operacionais, a verificação da assinatura do arquivo de hashes pode ser obtida em um terminal através do seguinte comando:

```bash
gpg --verify hashes.txt
```

Se o arquivo for autêntico, o resultado será algo parecido com isso:

```bash
gpg:                using RSA key 81AC591FE9C4B65C5806AFC3F0AF4D462A0BDF92
gpg: Good signature from "binaryFate <binaryfate@getmonero.org>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 81AC 591F E9C4 B65C 5806  AFC3 F0AF 4D46 2A0B DF92
```

Se o resultado exibir **Assinatura válida (Good signature)**, como no exemplo, então você pode continuar.

Se o resultado exibir **Assinatura inválida (BAD signature)**, **NÃO CONTINUE.** Delete o arquivo `hashes.txt` e volte para a [seção 3.1](#31-baixar-o-arquivo-de-hashes).

## 4. Baixar e verificar o binário

Nessa seção você irá aprender como baixar o binário do Monero para o seu sistema operacional, como obter o hash `SHA256` do arquivo binário que você baixou e como verificar se ele está correto.

### 4.1. Baixar o binário do Monero

No Windows ou Mac, vá para [getmonero.org](https://getmonero.org/downloads/) e baixe o arquivo correto para o seu sistema operacional. Salve o arquivo em seu computador. **Não extraia os arquivos ainda.**

No Linux, você pode baixar a carteira GUI usando o seguinte comando:

```bash
wget -O monero-gui-linux-x64-v0.16.0.0.tar.bz2 https://downloads.getmonero.org/gui/linux64
```

### 4.2. Verificação de binário no Linux ou Mac

Os passos para o Linux e para o Mac são os mesmos. Em um terminal, obtenha o hash `SHA256` do binário do Monero que você baixou. Como exemplo, esse guia irá utilizar o binário do GUI para `Linux, 64bit`. Substitua `monero-gui-linux-x64-v0.16.0.0.tar.bz2` com o nome do binário que você baixou na [seção 4.1](#41-baixar-o-binário-do-monero).

```bash
shasum -a 256 monero-gui-linux-x64-v0.16.0.0.tar.bz2
```

O resultado será algo parecido com o exibido abaixo, mas será diferente para cada arquivo binário. O hash `SHA256` do arquivo binário em seu computador deve corresponder ao hash que está listado para o seu binário no arquivo `hashes.txt`.

```bash
6a1064e89d1ac626ae9307429276ad447ddbbf38035de0b24599e267977e61f1  monero-gui-linux-x64-v0.16.0.0.tar.bz2
```

Se o hash **CORRESPONDE**, então você pode prosseguir! Extraia os arquivos e prossiga com a instalação.

Se o hash **NÃO CORRESPONDE**, **NÃO CONTINUE.** Delete o binário que você baixou e volte para a [seção 4.1](#41-baixar-o-binário-do-monero).

### 4.3. Verificação do binário no Windows

Em um terminal, obtenha o hash `SHA256` do binário do Monero que você baixou. Como exemplo, esse guia irá usar o binário do GUI para `Windows, 64bit`. Substitua `monero-gui-install-win-x64-v0.16.0.0.exe` com o nome do binário que você baixou na [seção 4.1](#41-baixar-o-binário-do-monero).

```bash
certUtil -hashfile monero-gui-install-win-x64-v0.16.0.0.exe SHA256
```
O resultado será algo parecido com o exibido abaixo, mas será diferente para cada arquivo binário. O hash `SHA256` do arquivo binário em seu computador deve corresponder ao hash que está listado para o seu binário no arquivo `hashes.txt`.

```bash
SHA256 hash of file monero-gui-install-win-x64-v0.16.0.0.exe:
08 20 ae b3 0b 39 bb 86 b5 50 ff 5d 6e 64 1c 16 49 1c ff 3c ff 2b 9a b9 87 3b dc 05 ac c6 a0 41
CertUtil: -hashfile command completed successfully.
```

Se o hash **CORRESPONDE**, então você pode prosseguir! Execute o arquivo e prossiga com a instalação.

Se o hash **NÃO CORRESPONDE**, **NÃO CONTINUE.** Delete o binário que você baixou e volte para a [seção 4.1](#41-baixar-o-binário-do-monero).

> Caso precise de ajuda para seguir estes passos, entre em contato conosco deixando seu comentário abaixo.

*Artigo traduzido e adaptado de [getmonero.org](https://getmonero.org/).*
