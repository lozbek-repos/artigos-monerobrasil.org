---
title: "Contribua com a comunidade Monero Brasil"
description: "Um guia passo-a-passo de como colaborar com a comunidade Monero Brasil"
date: 2020-05-16T06:50:53-03:00
draft: false
tags:
  - guia
  - contribuir
  - 
categories:
  - todos
  - guias
  -
image: "contribua.jpg"
author: "netrik182"
toc: true
---

A intenção deste site é a colaboração entre os membros da comunidade Monero no Brasil e demais países falantes da língua portuguesa. Caso queira contribuir enviando algum artigo ou tradução, é muito simples. Basta seguir os passos descritos neste post.

Todos os artigos do Monero Brasil estão hospedados em um repositório no GitLab chamado [artigos-MoneroBrasil.org](https://gitlab.com/netrik182/artigos-monerobrasil.org) para acompanhamento e publicação do conteúdo. Você encontrará instruções detalhadas com fotos logo aqui no início para ajudá-lo ao usar o GitLab.

Para quem já está acostumado a trabalhar com o git, basta submeter um "Merge Request" com novo arquivo em formato `.md` na raiz do repositório. Não se esqueça de criar um novo branch e utilizar o "Front Matter" e layout do autor fornecidos abaixo.

Ao final do guia, um exemplo de post é fornecido para fins de referência e em caso de dúvidas, por favor deixe seu comentário.

## 1. GitLab

### 1.1 Criar uma conta

> Esta etapa será necessária somente uma vez. Se você já possui uma conta, fique a vontade para seguir para o [item 1.2](#12-fork-do-reposit%C3%B3rio).

Acesse o link do [GitLab](https://gitlab.com/users/sign_in#register-pane) e crie uma conta. Seguindo o link fornecido, você poderá acessar a página de cadastro. Registre-se para uma conta (use uma senha forte).

### 1.2 Fork do repositório

> Esta etapa será necessária somente uma vez. Se você já possui o "Fork" do repositório, pode ir para o [item 2](#2-criar-um-novo-arquivo-para-o-post).

Navegue para o repositório [artigos-MoneroBrasil.org](https://gitlab.com/netrik182/artigos-monerobrasil.org), clique no botão "Fork" e selecione seu nome na tela seguinte.

{{< picture "fork.png" "fork.png" "Clique no botão Fork" >}}

Se tudo correr bem você verá uma faixa azul dizendo que seu "Fork" foi realizado com sucesso.

{{< picture "fork-success.png" "fork-success.png" "Clique no botão Fork" >}}

---

{{< picture "repo.jpg" "repo.jpg" "Tela do repositório" >}}

<!-- ### 1.3 Criar um novo "branch"

Para quem não está acostumado com desenvolvimento, esta etapa pode parecer não fazer sentido algum. Porém, para maior organização e controle por parte dos mantenedores, por favor seguia este passos mesmo que não entenda o motivo.

> Crie um novo branch toda vez que for submeter um post para o site.

{{< picture "branch.png" "branch.png" "Clique no botão Fork" >}}

Em seguida escolha um nome e clique em "Create branch".

{{< picture "create.png" "create.png" "Clique no botão Create branch" >}} -->

## 2. Criar um novo arquivo para o post

Na página principal, crie um novo arquivo clicando em "Web IDE". O nome do arquivo deverá ser o título do seu post, usando hífens **`(-)`** como espaço e extensão `.md`.

{{< picture "ide.png" "ide.png" "Abrir Web IDE" >}}

---

{{< picture "file.png" "file.png" "Criar novo arquivo" >}}

---

{{< picture "title.png" "title.png" "Exemplo de título" >}}

### 2.1 Configurar o "Front Matter"

A parte superior deste novo arquivo deve obrigatoriamente conter o "Front Matter", que nada mais é do que uma série de instruções que determinam o layout da página. Copie e cole exatamente como no exemplo abaixo e somente substitua as informações dos campos "title", "date" e "author".

```yml
---
title: "Exemplo de Título"
description: ""
date: 2020-05-18
draft: true
tags:
  -
  -
categories:
  - todos
  -
image: "thumbnail.svg"
author: ""
toc: false
---
```

`title:` -> **[obrigatório]** Título do seu post entre aspas

`description:` -> **[obrigatório]** Breve descrição do seu artigo. Uma ou duas linhas objetivas.

`date:` -> **[obrigatório]** Data atual no formato AAAA-MM-DD

`author:` -> [opcional] Seu nome de usuário, somente letras minúsculas, números e sem espaços

{{< picture "front-matter.png" "front-matter.png" "Colocar o Front Matter" >}}

Logo abaixo do Front Matter, ou seja, a partir da linha 16, comece a redigir todo o seu texto usando a [formatação Markdown](../../artigos/como-usar-a-formatação-markdown/).

A ferramenta [StackEdit](https://stackedit.io/app#) permite editar arquivos **markdown** de maneira muito simples e intuitiva e ainda conta com botões para editar seu texto com visualização em tempo real. Você poderá escrever seu post lá e depois copiar e colar o conteúdo no seu arquivo do GitLab.

Após ter colado o conteúdo, uma dica para visualizar a formatação e certificar-se que seu texto ficou da maneira que você esperava, é clicar na aba "Preview Markdown" dentro do GitLab mesmo.

{{< picture "preview.png" "preview.png" "Visualizar a formatação" >}}

## 3. Finalizar o Post

Pronto, agora que você já tem tudo em mãos é só enviar seu texto para nós clicando em "Commit..." e inserir o título do seu post na mensagem do commit (Commit Message).

{{< picture "commit.png" "commit.png" "Enviar texto" >}}

---

> Certifique-se que as opções "Create a new branch" e "Start a new merge request" estejam marcadas.

{{< picture "commit2.png" "commit2.png" "Enviar2 texto" >}}

### 3.1 Definir o autor (opcional)

Após e clicar em Commit, se quiser inserir suas informações "pessoais" e ser creditado como autor, copie o conteúdo abaixo e cole na descrição da "Merge Request", conforme mostrado na [figura do item 3.2](#32-enviar-o-post-para-ser-publicado).

> Copie todo o conteúdo, incluindo os sinais `---` do início e do final `---`.

```yml
---
- name: "netrik182"
  fullName: "netrik182"
  photo: "netrik182.png"
  url: "https://github.com/netrik182"
  bio: "It's me!"
---
```

`name:` -> **[obrigatório]** Coloque um nome curto, somente letras minúsculas, números e sem espaços

`fullName:` -> [opcional] Seu nome ou apelido que quer ser chamado, se não colocar nada será usado o nome fornecido acima

`photo:` -> [opcional] Uma foto sua ou de seu "usuário" nas redes sociais (se houver)

`url:` -> [opcional] Link para seu perfil no Twitter, LinkedIn, GitHub ou qualquer outro site

`bio:` -> [opcional] Uma breve descrição sobre você, seus hobbies ou qualquer coisa parecida.

### 3.2 Enviar o Post para ser publicado

> Certifique-se que as opções "Delete source branch when merge request is accepted" e "Squash commits when merge request is accepted" estejam marcadas.

{{< picture "merge-request.png" "merge-request.png" "Publicar!" >}}

## 4. Exemplo de Post e Aprovação

Pronto, se você seguiu corretamente todas as informações deste guia seu texto irá gerar uma nova página assim que for aprovado.

Muito obrigado pela sua contribuição! Fique atento ao seu email cadastrado no GitLab pois você receberá uma notificação caso alguma alteração ou correção em seu texto seja necessária.

Exemplo de post de [Como usar a formatação Markdown](../../artigos/como-usar-a-formatação-markdown/).
