---
title: "Como usar a Carteira Monero CLI"
description: "Passo-a-passo de como usar a carteira oficial do Monero para linha de comando no Windows, Linux e MacOS"
date: 2020-05-14T21:27:08-03:00
draft: true
tags:
  - guia
  - carteira
  -
categories:
  - todos
  - guias
  - carteiras
  -
image: "thumbnail.svg"
author: ""
toc: true
---
