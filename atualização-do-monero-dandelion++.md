---
title: "Atualização do Monero: Dandelion++"
description: "Monero adiciona mais uma camada de privacidade ao implementar a tecnologia Dandelion++"
date: 2020-05-21T16:48:14-03:00
draft: false
tags:
  - atualização
  - notícia
  -
categories:
  - todos
  - notícias
  - 
image: "dandelion.jpg"
author: "netrik182"
toc: false
---

## Dandelion++

Dandelion++ é uma melhoria de privacidade desenvolvida por pesquisadores da Universidade de Illinois. Esta tecnologia altera a forma como as transações se propagam através da rede Monero na tentativa de dificultar a associação de uma transação ou grupo de transações a um endereço IP específico.

A melhoria "Dandelion" foi originalmente desenvolvida para o Bitcoin, mas os desenvolvedores do Monero saíram na frente para implementá-lo. O código Dandelion++ acaba de ser integrado com sucesso na base de código Monero e deverá ser incluído já na próxima atualização dos softwares Monero, prevista para este mês. Ou ponto positivo desta melhoria é que ela não exigirá um _hard-fork_, pois não altera nenhuma regra do consenso. Trata-se de uma atualização opcional, porém recomendada.

### Como funciona o Monero atualmente

Assim como no Bitcoin, os nós no Monero comunicam-se uns com os outros usando o mesmo protocolo TCP/IP[^1] usado pelo seu navegador para acessar esta página. As carteiras Monero se comunicam com pelo menos um desses nós para acesso à rede. Você pode optar por rodar seu próprio nó local ou acessar um nó remoto.

[^1]: Protocolo [TCP/IP](https://pt.wikipedia.org/wiki/Transmission_Control_Protocol)

Hoje, um nó Monero que transmite a transação de uma carteira inicia um processo de propagação chamado _"flooding"_ (transbordar, em inglês). Assim, ele comunica a transação a todos os seus pares, depois todos esses pares se comunicam com todos os seus pares, e assim por diante. Infelizmente, é possível vincular uma transação a um endereço IP utilizando a implementação atual. Essa associação não é fácil e requer uma certa sofisticação técnica e trabalho duro, mas com motivação, habilidade e recursos suficientes, ela pode ser feita.

Uma equipe de pesquisadores de Luxemburgo, Austrália e Hong Kong demonstrou no final de 2019 que seria possível implantar alguns nós Monero para descobrir todos os nós da rede e suas interconexões. Ao mapear a topologia da rede, esses nós maliciosos teriam a habilidade de determinar a origem de uma transação e seu endereço IP, deduzindo assim em quais nós cada transação foi originada.

Dandelion++ é um método de propagação utilizado para fazer com que este tipo de associação entre o endereço IP e transação seja quase impossível.

### Monero juntamente com Dandelion++

A ideia central do Dadelion++ é primeiro encaminhar as transações para um nó aleatório de forma não-rastreável e depois realizar o "transbordo" da transação também de forma aleatória. O nome Dandelion, que em português significa dente-de-leão, ajuda a dar uma visualização aproximada desta ideia ao se imaginar um dente-de-leão sendo soprado, com todas as suas pétalas voando aleatoriamente.

O nó que tenta transmitir uma transação procurará por um nó intermediador[^2] para transmiti-la. Esta conexão os nós se parece com uma "haste" e por isso é chamada de "fase em haste" ou _"stem phase"_, em inglês. Em seguida, o nó intermediador, ao receber a transação, transmite-a de tal forma que o fluxo de informação cria uma figura parecida com um dente-de-leão. Esta etapa é conhecida como fase _"fluff"_. A fase em haste é também chamada de "fase de anonimato" e a fase fluff é também chamada de "fase de propagação". Apenas formas diferentes de se referir ao mesmo processo.

[^2]: Intermediador ou [proxy](https://pt.wikipedia.org/wiki/Proxy)

A fase em haste atinge o anonimato através da conectividade dinâmica, ou seja, os nós mudam de conexão a cada poucos minutos. Cada intervalo de tempo entre as trocas é chamado de "época". No início de cada época, os nós selecionam duas novas conexões ao acaso para retransmitir as transações. Sempre que o nó tem uma nova transação que precisa transmitir, ele escolhe um dos dois nós de retransmissão e permanece com aquele nó durante toda aquela época. Após o fim de uma época, começa uma nova e o nó escolhe outros dois nós para se conectar.

Além da troca dinâmica, a cada época, um nó se designa retransmissor ou difusor. Um retransmissor irá encaminhar as transações recebidas para um nó intermediador (fase em haste ou de anonimato) e um difusor irá iniciar o processo de difusão (fase fluff ou de propagação). Durante a fase de difusão o tempo de comunicação entre os nós é aleatório, tornando difícil para nós maliciosos localizarem a origem da transação e, portanto, o endereço IP correspondente.

O último componente do Dandelion++ é chamado de "mecanismo a prova de falhas" (_fail-safe_, em inglês). Cada nó que retransmite uma transação durante a fase em haste inicia um temporizador para essa transação. Se um certo tempo passa sem que o nó receba a transação por meio da fase de propagação, ele iniciará a  sua própria. Este mecanismo protege a transação de duas coisas: **(a)** a transação ser jogada fora por nós maliciosos que podem descartar a transação durante a fase em haste e **(b)** frustra as tentativas de desanonimização pois a origem da transação raramente é aquela também responsável por propaga-la.

### Conclusões

Com estes mecanismos em ação, o resultado é um método rápido, eficiente e eficaz para resistir aos adversários de grande escala que tentam criar associações entre endereços IP e transações. Com a eminente inclusão do Dandelion++ no Monero, uma enorme rede de _bots_ maliciosos participando na rede Monero não poderá mais associar transações com endereços IP de forma confiável.

> E você, o que achou dessa nova tecnologia que foi adicionada ao Monero? Qual outro recurso gostaria de ver implementado no futuro? Deixe seu comentário abaixo!

*Artigo traduzido e adaptado de [edge.app](https://edge.app/).*
