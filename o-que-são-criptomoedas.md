---
title: "O que são Criptomoedas?"
description: "Criptomoeda é uma moeda digital, mas não as confunda com moedas virtuais ou 'tokens'."
date: 2020-12-01T01:28:56-03:00
draft: false
tags:
  - conceito
  -
categories:
  - todos
  - conceitos
  -
image: "criptomoedas.jpg"
author: ""
toc: false
---

## Afinal, o que são 'criptomoedas'?

Criptomoeda é uma moeda digital na qual técnicas de criptografia são utilizadas para regular a geração de unidades monetárias e verificar a transferência de fundos, geralmente operando independentemente de um banco central.

Criptomoeda (_criptocurrency_, em inglês) é o termo genérico para um grande conjunto de ativos digitais que utilizam técnicas de criptografia para gerar unidades de moeda, verificar as transações e transferir valor. Geralmente, as criptomoedas são consideradas descentralizadas.

Uma criptomoeda não deve ser confundida com a moeda virtual, que é um tipo de dinheiro digital que é normalmente controlado por seus criadores ou desenvolvedores. Alguns exemplos de moeda virtual são programas de pontos de recompensa de cartão de crédito ou o outros, que podem ser trocados por moeda ou valor em dinheiro, mas não são consideradas criptomoedas porque são centralizadas e controladas/emitidas por uma única entidade.
