---
title: "Brasileiro cria sistema que minera Monero rodando direto do pendrive"
description: "Com a proposta de simplificar esse processo e oferecer a oportunidade para quem quer testar a mineração usando a CPU de seu computador, um brasileiro desenvolveu o MineCore, um sistema que já contém o que é preciso para iniciar."
date: 2021-10-19
draft: true
tags:
  -
  -
categories:
  - todos
  -
image: "MineCore_thumbs.png"
author: "jeff"
toc: false
---


# Brasileiro cria sistema que minera Monero rodando direto do pendrive


Já foi o tempo que mineração de criptomoedas precisava ser algo complicado, que requer hardware avançado e grande conhecimento técnico. Com a proposta de simplificar esse processo e oferecer a oportunidade para quem quer testar a mineração usando a CPU de seu computador, um brasileiro desenvolveu o **MineCore**, um sistema que já contém o que é preciso para iniciar. E melhor ainda: sem precisar instalar nada, ja que o sistema roda integralmente a partir do boot pelo pendrive.

O desenvolvedor, que atende pelo apelido de *Jeff* na comunidade Monero Brasil, ressalta que criou o software pensando em rodar testar o desempenho de equipamentos mais antigos em mineração. Por isso o MineCore é totalmente baseado na distribuição Linux Tinycore ([http://tinycorelinux.net/](http://tinycorelinux.net/)). O Linux Tinycore é uma velha distribuiçao conhecida da comunidade Linux por apresentar uma das menores e mais leves distribuições ainda em desenvolvimento (atualmente na versão 12).

O sistema todo é baseado na versão de 64bits do Tinycore (*CorePure 64*). Mesmo incluindo pacotes opcionais como navegador web, gerenciador de arquivos e gerenciador de redes sem fio — além dos scripts de configuração da mineração — cabe numa imagem de menos de 200mb, podendo ser colocado em pendrives de até 512mb. Para fazer o download da imagem do sistema, basta acessar a página [https://www.minecore.live](https://www.minecore.live). No GitHub ([https://github.com/MineCoreLive/minecore](https://github.com/MineCoreLive/minecore)) voce podem conferir e acessar os códigos fontes dos scripts incluidos no sistema.

Para facilitar a operação e poder se ajustar ao boot em qualquer máquina, o MineCore não vem pré-carregado com o minerador mais popular de Monero, o XMRig. Ao invés disso, usa uma estratégia diferente: vem com um shell script que, quando executado, baixa uma versão binária do XMRig diretamente da pool [MoneroOcean](https://moneroocean.stream). Isso torna possível a execução com a calibração própria para cada computador e usando sempre uma versão atualizada do minerador.

O Tinycore original, por si só, já é capaz de executar o minerador em sua forma binária. O desafio do "fork" MineCore foi recompilar um kernel próprio, ativando configurações que não são padrão na distro para permitir o máximo desempenho do minerador em qualquer equipamento. Isso foi somado à vantagem do sistema em usar uma quantidade extremamente limitada de recursos e não precisar de um HD ou SSD instalados.

Com isso é possível usar todo o potencial de virtualmente qualquer máquina para mineração, mesmo das mais antigas. E isso é feio sem abrir mão da praticidade e facilidade de usar uma interface gráfica simples — mas poderosa — que vai ajudar aqueles que querem ou precisam de um pouco mais de comodidade na hora de editar um endereço de carteira ou executar corretamente um código, já que telas de terminal podem ser bastante intimidadoras para usuários leigos.

## Usando o MineCore para minerar

A primeira medida é instalar o MineCore em um dispositivo USB. Para isso pode ser usado qualquer criador de USB de boot disponível na Internet, como o [Lili USB Creator](https://www.linuxliveusb.com/) ou [UnebootIn](https://unetbootin.github.io), recomendados pelo desenvolvedor. Na maioria dos casos será preciso dar boot ativando na bios o modo Legado (ou Legacy) da máquina, por limitações do próprio Tinycore com sistema UEFI.

Ao ser inicializado, o MineCore exibe os arquivos de instruções e scripts shell de configuração e início. O primeiro procedimento necessário, caso o usuário esteja usando uma rede wireless, é a configuração da rede, no ícone correspondente. Em caso de rede cabeada, a conexão deve ser automática.

A seguir é preciso o preenchimento do arquivo carteira.txt, com a inclusão de um endereço válido de carteira Monero para onde o resultado da mineração será enviado. O MineCore usa um gerenciador gráfico extremamente leve e simples, mesmo assim bastante útil nesta hora, para usar o navegador pré instalado e acessar uma anotação online com seu endereço de carteira, por exemplo.

Ao baixar o XMRig customizado diretamente do repositório da pool [MoneroOcean](https://moneroocean.stream/), é preciso rodar a mineração pela primeira vez. O sistema automaticamente vai se calibrar e configurar para minerar pelo algoritmo mais eficiente para a máquina e para o momento, buscando entregar o máximo de rentabilidade para o usuário.

Previsto para rodar em qualquer máquina *on-the-fly*, por padrão o MineCore não guarda as configurações. Mas no arquivo de instruções são descritas as etapas para quem quiser tornar as configurações persistentes e configurar a mineração para executar automaticamente nos próximos boots do computador pelo pendrive.
