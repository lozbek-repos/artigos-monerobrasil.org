---
title: "O que é Monero (XMR)?"
description: "O Monero é a principal criptomoeda com foco em transações privadas."
date: 2020-12-01T00:59:57-03:00
draft: false 
tags:
  - conceito
  -
categories:
  - todos
  - conceitos
  - 
image: "as-monero.jpg"
author: ""
toc: false
---

## O que é o Monero (XMR)?

O Monero é a principal criptomoeda com foco em transações privadas e resistentes à censura.

A maioria das criptomoedas existentes, incluindo Bitcoin e Ethereum, têm blockchains transparentes, o que significa que as transações são livremente consultadas e rastreáveis por qualquer pessoa no mundo. Além disso, os endereços de envio e recebimento dessas transações podem e muitas vezes são vinculados à identidade de uma pessoa na vida real.

> As transações do Monero são **confidenciais** e **não rastreáveis**.

Toda transação do Monero, por padrão, ofusca os endereços de envio e recebimento, bem como os valores transacionados. A privacidade obrigatória faz com que a atividade de cada usuário do Monero aumente ainda mais a privacidade de todos os outros usuários, ao contrário das criptomoedas seletivamente transparentes (por exemplo, Zcash).

O Monero é fungível. Em virtude da sua ofuscação, nenhuma moeda pode ser marcada pela através de suas transações anteriores. Isso significa que o Monero sempre será aceito sem o risco de ser censurado.

O Monero é uma comunidade raiz que atrai os melhores pesquisadores de criptomoedas e talentos de engenharia do mundo.

Mais de 420 desenvolvedores contribuíram ao Projeto Monero, incluindo 30 desenvolvedores core. Fóruns e outros canais de comunicação são ativos e receptivos.

O Monero é dinheiro eletrônico que permite pagamentos rápidos e baratos para qualquer lugar do mundo.
